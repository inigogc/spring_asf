package es.deusto.asf.springLaboratory.concursoDeTalentos.artistas;

import es.deusto.asf.springLaboratory.concursoDeTalentos.ActuacionException;
import es.deusto.asf.springLaboratory.concursoDeTalentos.instrumentos.Instrumento;

public class Instrumentista  extends ArtistaImpl {

    private String cancion;
    private Instrumento instrumento;

    @Override
    public void actuar() throws ActuacionException {
        System.out.println("Tocando la cancion " + cancion + ":");
        instrumento.tocar();
    }

    public String getCancion() {
        return cancion;
    }

    public void setCancion(String cancion) {
        this.cancion = cancion;
    }

    public Instrumento getInstrumento() {
        return instrumento;
    }

    public void setInstrumento(Instrumento instrumento) {
        this.instrumento = instrumento;
    }



    public Instrumentista (String nombre, String actuacion){
        super(nombre, actuacion);
    }
}
