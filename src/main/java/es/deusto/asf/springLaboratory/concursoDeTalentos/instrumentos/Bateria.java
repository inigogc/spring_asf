package es.deusto.asf.springLaboratory.concursoDeTalentos.instrumentos;

public class Bateria implements Instrumento{

    @Override
    public void tocar() {
        System.out.println("TUPA TUPA TUPA");
    }
}
