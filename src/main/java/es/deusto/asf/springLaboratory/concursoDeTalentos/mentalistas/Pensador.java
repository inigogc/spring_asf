package es.deusto.asf.springLaboratory.concursoDeTalentos.mentalistas;

public interface Pensador {
    void pensarSobreAlgo(String pensamientos);
}
