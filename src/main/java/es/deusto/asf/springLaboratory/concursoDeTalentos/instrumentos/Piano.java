package es.deusto.asf.springLaboratory.concursoDeTalentos.instrumentos;

public class Piano implements Instrumento {

    @Override
    public void tocar() {
        System.out.println("PLIN PLIN PLIN");
    }
}
