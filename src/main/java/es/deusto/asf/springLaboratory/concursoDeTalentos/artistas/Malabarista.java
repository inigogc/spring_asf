package es.deusto.asf.springLaboratory.concursoDeTalentos.artistas;

import es.deusto.asf.springLaboratory.concursoDeTalentos.ActuacionException;

public class Malabarista  extends ArtistaImpl{
    private int numeroDeBolas = 3;

    public Malabarista (String nombre, String actuacion){
        super(nombre, actuacion);
    }

    public Malabarista (String nombre, String actuacion, int numeroDeBolas){
        super(nombre, actuacion);
        this.numeroDeBolas = numeroDeBolas;
    }

    @Override
    public void actuar() throws ActuacionException {
        System.out.println("Haciendo malabares con " + numeroDeBolas + " bolas");
    }
}
