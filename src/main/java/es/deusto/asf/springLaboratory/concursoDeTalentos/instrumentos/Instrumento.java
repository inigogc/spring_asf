package es.deusto.asf.springLaboratory.concursoDeTalentos.instrumentos;

public interface Instrumento {
    public void tocar();
}
