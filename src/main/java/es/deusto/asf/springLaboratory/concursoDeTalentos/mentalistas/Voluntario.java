package es.deusto.asf.springLaboratory.concursoDeTalentos.mentalistas;

public class Voluntario implements Pensador {

    private String pensamientos;

    @Override
    public void pensarSobreAlgo(String pensamientos) {
        this.pensamientos = pensamientos;
    }

    public String getPensamientos() {
        return pensamientos;
    }

    public Voluntario() {
    }
}
