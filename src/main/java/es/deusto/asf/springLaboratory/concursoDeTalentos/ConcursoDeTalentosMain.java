package es.deusto.asf.springLaboratory.concursoDeTalentos;

import es.deusto.asf.springLaboratory.concursoDeTalentos.DAO.DAO;
import es.deusto.asf.springLaboratory.concursoDeTalentos.artistas.Artista;
import es.deusto.asf.springLaboratory.concursoDeTalentos.mentalistas.Mentalista;
import es.deusto.asf.springLaboratory.concursoDeTalentos.mentalistas.Pensador;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class ConcursoDeTalentosMain {

    public static void main(String args[]){
        ApplicationContext context = new ClassPathXmlApplicationContext("concursoDeTalentos.xml");

        List<Artista> artistas = new ArrayList<>();

        Escenario escenario = (Escenario)context.getBean("escenario");

        DAO dao = (DAO)context.getBean("dao");

        artistas.add((Artista)context.getBean("aitor"));
        artistas.add((Artista)context.getBean("josu"));
        artistas.add((Artista)context.getBean("andrea"));
        artistas.add((Artista)context.getBean("unai"));
        artistas.add((Artista)context.getBean("andoni"));

        try {
            for (Artista artista:artistas
                 ) {
                artista.actuar();
                dao.insert(artista);
            }
        } catch (ActuacionException a) {
            System.out.println("Fallo en la actuación");
        }

        Pensador voluntario = (Pensador)context.getBean("voluntario");
        Mentalista patrick = (Mentalista)context.getBean("patrick");

        voluntario.pensarSobreAlgo("Dos más dos son cuatro");

        System.out.println("Los pensamientos del voluntarios son: " + patrick.getPensamientos());

        ((ClassPathXmlApplicationContext) context).close();
    }
}
