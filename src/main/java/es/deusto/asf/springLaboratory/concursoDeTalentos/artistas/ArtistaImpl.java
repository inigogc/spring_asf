package es.deusto.asf.springLaboratory.concursoDeTalentos.artistas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="actuaciones")
public abstract class ArtistaImpl implements Artista {

    @Column(name="nombre")
    private String nombre;
    @Column(name="actuacion")
    private String actuacion;

    public ArtistaImpl(String nombre, String actuacion) {
        this.nombre = nombre;
        this.actuacion = actuacion;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String getActuacion() {
        return actuacion;
    }

    public void setActuacion(String actuacion) {
        this.actuacion = actuacion;
    }
}
