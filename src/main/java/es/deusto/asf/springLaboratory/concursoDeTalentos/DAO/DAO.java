package es.deusto.asf.springLaboratory.concursoDeTalentos.DAO;

import es.deusto.asf.springLaboratory.concursoDeTalentos.artistas.Artista;

public interface DAO {
    	public void insert(Artista artista);
}
