package es.deusto.asf.springLaboratory.concursoDeTalentos.artistas;

import es.deusto.asf.springLaboratory.concursoDeTalentos.ActuacionException;

public interface Artista {
    public void actuar() throws ActuacionException;
    public String getNombre();
    public String getActuacion();
    public void setNombre(String nombre);
    public void setActuacion(String actuacion);
}
