package es.deusto.asf.springLaboratory.concursoDeTalentos.mentalistas;

public class Mago implements Mentalista {

    private String pensamientos;

    @Override
    public String getPensamientos() {
        return pensamientos;
    }

    @Override
    public void leerLaMente(String pensamientos) {
        System.out.println("Leyendo la mente del voluntario");
        this.pensamientos = pensamientos;
    }

    public Mago() {
    }
}
