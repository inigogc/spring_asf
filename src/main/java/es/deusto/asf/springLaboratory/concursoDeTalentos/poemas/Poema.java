package es.deusto.asf.springLaboratory.concursoDeTalentos.poemas;

public interface Poema {
    public void recitar();
}
