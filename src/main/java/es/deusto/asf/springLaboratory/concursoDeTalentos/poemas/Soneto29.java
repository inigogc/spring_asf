package es.deusto.asf.springLaboratory.concursoDeTalentos.poemas;

public class Soneto29 implements Poema{
    private static final String[] LINES = {
            "When, in disgrace with fortune and men's eyes" };

    public Soneto29(){}

    public void recitar() {
        for (String LINE : LINES) {
            System.out.println(LINE);
        }
    }
}
