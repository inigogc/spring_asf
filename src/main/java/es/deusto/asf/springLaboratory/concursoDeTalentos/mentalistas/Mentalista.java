package es.deusto.asf.springLaboratory.concursoDeTalentos.mentalistas;

public interface Mentalista {
    void leerLaMente(String pensamientos);
    String getPensamientos();
}
