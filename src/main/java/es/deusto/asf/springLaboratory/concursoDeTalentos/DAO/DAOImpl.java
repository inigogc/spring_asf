package es.deusto.asf.springLaboratory.concursoDeTalentos.DAO;

import es.deusto.asf.springLaboratory.concursoDeTalentos.artistas.Artista;
import org.springframework.jdbc.core.JdbcTemplate;

public class DAOImpl implements DAO {
    JdbcTemplate jdbcTemplate;

    public DAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void insert(Artista artista) {
        jdbcTemplate.update("INSERT INTO actuaciones(nombre, actuacion) values (?,?)",
                artista.getNombre(), artista.getActuacion());
    }
}
