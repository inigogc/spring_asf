package es.deusto.asf.springLaboratory.concursoDeTalentos;

import org.aspectj.lang.ProceedingJoinPoint;

public class Publico {

    private void tomarAsiento() {
        System.out.println("El público está tomando asiento");
    }

    private void apagarMoviles() {
        System.out.println("El público ha apagado sus móviles");
    }

    private void aplaudir() {
        System.out.println("PLAS PLAS PLAS PLAS");
    }

    private void abuchear() {
        System.out.println("¡¡Buuuuh!! ¡¡Devolvednos el dinero!!");
    }

    private void mostrarDuracion(long duracion){
        System.out.println("La actuación ha durado " + duracion + "milisegundos");
    }

    public void verActuacion(ProceedingJoinPoint joinPoint) {
        try {
            tomarAsiento();
            apagarMoviles();
            long comienzo = System.currentTimeMillis();

            joinPoint.proceed();

            long duracion = System.currentTimeMillis() - comienzo;

            aplaudir();
            mostrarDuracion(duracion);

        } catch (Throwable t) {
            abuchear();
        }
    }
}
