package es.deusto.asf.springLaboratory.concursoDeTalentos.artistas;

import es.deusto.asf.springLaboratory.concursoDeTalentos.ActuacionException;
import es.deusto.asf.springLaboratory.concursoDeTalentos.poemas.Poema;

public class MalabaristaPoeta extends Malabarista {
    private Poema poema;

    public MalabaristaPoeta(String nombre, String actuacion, Poema poema) {
        super(nombre, actuacion);
        this.poema = poema;
    }


    public MalabaristaPoeta(String nombre, String actuacion, Poema poema, int bolas) {
        super(nombre, actuacion, bolas);
        this.poema = poema;
    }

    @Override
    public void actuar() throws ActuacionException {
        super.actuar();
        System.out.println("Mientras recito...");
        poema.recitar();
    }
}
