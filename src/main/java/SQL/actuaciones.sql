DROP TABLE IF EXISTS `actuaciones`;
CREATE TABLE `actuaciones` (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(45),
  actuacion varchar(100)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;